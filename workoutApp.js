var express = require("express");
var http = require("http");
var app = express();
var bodyParser = require('body-parser');
var mongoose = require("mongoose");
var moment = require('moment');
var workoutSchema, Workout;

//Setting up connection to Database and establisching Schema
mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.once('open', function(){
    workoutSchema = new mongoose.Schema({
        distance: {type: Number, default: 0, min: 0},
        goal: {type: Number, default: 0, min: 0},
        date: {type: Date, default: moment()},
        complete: {type: Boolean, default: false}
    });
    Workout = mongoose.model('Workout', workoutSchema);
});

//Needed for parsing requests
app.use(bodyParser.urlencoded({
    extended: true
}));


//**********************************************************************************************************************
//react to request for creation of a new workout
//Parameters that can be passed are 'distance', 'goal' and 'date'
//Any parameter that is not passed in the request will be set to its default value defined in the Schema
//Gives the internal ID of the just created workout as a response
//**********************************************************************************************************************
app.post('/workout', function(req, res){
	if(typeof(req.body.date) != 'undefined' && moment(req.body.date).isAfter(moment())){ //Check for invalid date
		res.writeHead(500, {'Content-Type': 'text/plain'});
    res.end("Date can't be in the future!\n");
    return;
	}
	
   var work = new Workout({ //create new workout
   		distance: typeof(req.body.distance) != 'undefined' && req.body.distance > -1 ? req.body.distance : this.distance, //check distance for legal values
   		goal: typeof(req.body.goal) != 'undefined' && req.body.goal > -1 ? req.body.goal : this.goal, //check goal or legal values
   		date: typeof(req.body.date) != 'undefined' ? req.body.date : this.date,	//check date for value
   		complete: typeof(req.body.date) != 'undefined' ? true : false	//if the date was set and not illegal it is a past entry so complete is set
   });
   work.complete = work.distance >= work.goal ? true : work.complete; //check if complete needs to be set due to running distance reaching the goal

   
   work.save(function(err, nWork){ //save created workout and respond with result + ID if successful
   	 if(err){
   	 		res.writeHead(500, {'Content-Type': 'text/plain'});
		 		res.end("Error occurred while trying to save new workout!\n");
   	 }
   	 else{
		 		res.writeHead(200, {'Content-Type': 'text/plain'});
		 		res.end("Workout successfully created with ID: " + work._id + "\n");
		 }
	 });
});

//**********************************************************************************************************************
//react to request for viewing or updating a workout
//Parameters that can be passed are 'id', 'distance', 'goal' and 'date'
//if only the id is passed the response will be a listing of all attributes of the corresponding workout if found
//if no id is passed the response is a listing of all attributes of all workouts in the database
//if the id and at least one of the other parameters are passed the corresponding workout is updated and returned in response
//**********************************************************************************************************************
app.post('/', function(req, res){
    if(typeof(req.body.id) == 'undefined'){	//check if id is present
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write('Complete list of workouts (to modify one provide its id):\n');
        Workout.find(function(err,workouts){	
        		if(err){
        			res.end("Error retrieving list!!!!!!!!!!");
        		}
        		else{
        			res.end("" + workouts);	//write all workouts into response
        		}
        });
        return;
    }
    
    Workout.findOne({_id: req.body.id}, function(err, found){	//try finding workout with passed id
    		if(err){
    				res.writeHead(404, {'Content-Type': 'text/plain'});
        		res.end("Couldn't find any Workout corresponding to that ID!\n");
        		return;
    		}
    		var justDisplay = true;	//variable for deciding if another parameter was passed
    		
    		if(typeof(req.body.distance) != 'undefined'){ //check if distance was passed
    			if(req.body.distance < found.distance){	//check if user tries to decrease distance
    					res.writeHead(500, {'Content-Type': 'text/plain'});
        			res.end("You are not allowed to decrease the recorded distance!\n");
        			return;
    			}
    			found.distance = req.body.distance;	//update distance
    			if(req.body.distance >= found.goal){	//check if workout is complete
    				found.complete = true;
    			}
    			justDisplay = false;
    		}
    		if(typeof(req.body.goal) != 'undefined'){	//check if goal was passed
    			found.goal = req.body.goal;	//update goal
    			if(found.distance >= req.body.goal){	//check if workout is complete
    				found.complete = true;
    			}
    			justDisplay = false;
    		}
    		if(typeof(req.body.date) != 'undefined'){	//check if date was passed
    			if(moment(req.body.date).isAfter(moment())){  //check for illegal date
    					res.writeHead(500, {'Content-Type': 'text/plain'});
        			res.end("How about you first actually DO that workout before bragging about it?\n");
        			return;
    			}
    			found.date = req.body.date;	//update date
    			justDisplay = false;
    		}
    		if(justDisplay){	//check if user just wants to display the workout
    					res.writeHead(200, {'Content-Type': 'text/plain'});
        			res.end("Here is the data on the Workout you requested:\n" + found.toString());
    		}
    		else{
						found.save(function(err, saved){	//try to save updated workout and respond with result
							if(err){
									res.writeHead(500, {'Content-Type': 'text/plain'});
				    			res.end("Something went wrong trying to update your workout :/ \n");
				    			return;
							}
							else{
									res.writeHead(200, {'Content-Type': 'text/plain'});
									res.end("Uhhh so you can't get enough exercise huh? Her is what your Workout looks like now:\n" + found.toString());
							}
						});
    		}
    		
    });
    
      
});

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Waiting for your sweaty results at http://%s:%s", host, port)
})
